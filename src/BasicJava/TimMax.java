package BasicJava;

import java.util.Scanner;

public class TimMax {
    public static int max3(int a, int b, int c) {
        if (a >= b && a >= c){
            return a;
        }
        if (b >= c) {
            return b;
        }
        return c;
    }

    public static void main(String[] args) {
        int a, b, c;
        Scanner input = new Scanner(System.in);
        System.out.println("Nhap a: ");
        a = input.nextInt();
        System.out.println("Nhap b: ");
        b = input.nextInt();
        System.out.println("Nhap c: ");
        c = input.nextInt();
        System.out.println("So lon nhat la: ");
        System.out.print(max3(a, b, c));
    }
}
